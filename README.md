# Prueba Microservicios
 Se generaron dos microservicios
 - Personas: url http://localhost:9090/
 - Nucleo Familiares: url http://localhost:9091/

### Tecnologias usadas!

  - SpringBoot
  - ORM: JPA - Hibernate 
  - Base de Datos: Mysql
  - Gradle
  - Servidor: Apache Tomcat
  - Swagger: Generacion de documentacion
  - lombok: Inyeccion de dependencias
  
**Nota**
- Las evidencias se encuentran en cada proyecto de los microservicios