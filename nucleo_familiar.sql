-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 08, 2020 at 07:09 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nucleo_familiar`
--

-- --------------------------------------------------------

--
-- Table structure for table `log_auditoria`
--

CREATE TABLE `log_auditoria` (
  `id` bigint(20) NOT NULL,
  `microservicio` varchar(50) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `endpoint` varchar(50) DEFAULT NULL,
  `http_code` int(11) DEFAULT NULL,
  `message` text,
  `type_log` varchar(20) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log_auditoria`
--

INSERT INTO `log_auditoria` (`id`, `microservicio`, `url`, `endpoint`, `http_code`, `message`, `type_log`, `create_at`) VALUES
(7, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'Ya esta asociado el familiar', 'WARNING', '2020-11-08 17:57:12'),
(9, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'Ya esta asociado el familiar', 'WARNING', '2020-11-08 18:04:36'),
(10, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'Ya esta asociado el familiar', 'WARNING', '2020-11-08 18:05:41'),
(11, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 500, 'No se pudo asociar la persona', 'ERROR', '2020-11-08 18:06:26'),
(12, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'No se encuentra ningun tipos de nucleo familiar', 'WARNING', '2020-11-08 18:07:40'),
(13, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'No se encuentra ningun tipos de nucleo familiar', 'WARNING', '2020-11-08 18:07:44'),
(14, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'No se encuentra ningun tipos de nucleo familiar', 'WARNING', '2020-11-08 18:08:47'),
(15, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'No se encuentra ningun tipos de nucleo familiar', 'WARNING', '2020-11-08 18:08:48'),
(16, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 200, 'Listado de tipos de nucleo familiar', 'INFO', '2020-11-08 18:10:30'),
(17, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 202, 'Ya esta asociado el familiar', 'WARNING', '2020-11-08 18:10:53'),
(18, 'personas', 'localhost:9090', '/persons', 200, 'Listado de personas', 'INFO', '2020-11-08 18:35:43'),
(19, 'personas', 'localhost:9090', '/persons', 200, 'Creacion de una persona', 'INFO', '2020-11-08 18:35:46'),
(20, 'personas', 'localhost:9090', '/persons', 202, 'Ya existe la persona', 'WARNING', '2020-11-08 18:35:52'),
(21, 'personas', 'localhost:9090', '/persons', 200, 'Creacion de una persona', 'INFO', '2020-11-08 18:35:54'),
(22, 'personas', 'localhost:9090', '/persons', 202, 'Ya existe la persona', 'WARNING', '2020-11-08 18:35:56'),
(23, 'personas', 'localhost:9090', '/persons', 200, 'Creacion de una persona', 'INFO', '2020-11-08 18:35:58'),
(24, 'personas', 'localhost:9090', '/persons/11', 200, 'Actualizacion de una persona', 'INFO', '2020-11-08 18:36:00'),
(25, 'personas', 'localhost:9090', '/persons/11', 200, 'Se Inhabilito de una persona', 'INFO', '2020-11-08 18:37:56'),
(26, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 200, 'Listado de tipos de nucleo familiar', 'INFO', '2020-11-08 18:39:56'),
(27, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 200, 'Se asocio correctamente', 'INFO', '2020-11-08 18:44:44'),
(28, 'nucleofamiliar', 'localhost:9091', '/nucleofamiliar', 200, 'Eliminar asocio con familia', 'INFO', '2020-11-08 18:45:58');

-- --------------------------------------------------------

--
-- Table structure for table `nucleo_familiar__asocio`
--

CREATE TABLE `nucleo_familiar__asocio` (
  `id_nucleo_familiar__asocio` int(11) NOT NULL,
  `id_persona_encargada` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nucleo_familiar__personas`
--

CREATE TABLE `nucleo_familiar__personas` (
  `id_nucleo_familiar__persona` int(11) NOT NULL,
  `documento_persona` varchar(50) NOT NULL,
  `nombre_persona` varchar(50) NOT NULL,
  `apellido_persona` varchar(50) NOT NULL,
  `correo_persona` varchar(70) DEFAULT NULL,
  `telefono_persona` varchar(20) DEFAULT NULL,
  `fecha_nac_persona` date NOT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_nucleo_familiar_tipo` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0' COMMENT '0 - No 1 - Si '
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nucleo_familiar__personas`
--

INSERT INTO `nucleo_familiar__personas` (`id_nucleo_familiar__persona`, `documento_persona`, `nombre_persona`, `apellido_persona`, `correo_persona`, `telefono_persona`, `fecha_nac_persona`, `create_at`, `id_nucleo_familiar_tipo`, `deleted`) VALUES
(1, '1070973178', 'Daniel Esteban', 'Chia Jaime', 'danieljaime308@gmail.com', '3057200288', '1995-04-27', '2020-11-07 14:18:18', 1, 0),
(2, '1060923134', 'Luisa Fernanda', 'Perez Perez', 'luisaperez20@gmail.com', '32012232', '1990-10-11', '2020-11-07 19:46:14', 2, 0),
(3, '1001233446', 'Felipe Prueba', 'Perez Perez', '', '', '2003-08-11', '2020-11-07 19:46:10', 3, 0),
(9, '123465232', 'Sara Prueba', 'Perez Perez', '', '', '2005-12-11', '2020-11-07 19:47:42', 3, 0),
(10, '97543221', 'Camila Prueba', 'Perez Perez', '', '', '2006-01-04', '2020-11-07 19:48:02', 3, 0),
(11, '341231', 'Marco', 'Lopez Lopez', 'marco203@gmail.com', '3110233123', '1972-07-19', '2020-11-08 18:36:45', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nucleo_familiar__tipo`
--

CREATE TABLE `nucleo_familiar__tipo` (
  `id_nucleo_familiar_tipo` int(11) NOT NULL,
  `nombre_tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nucleo_familiar__tipo`
--

INSERT INTO `nucleo_familiar__tipo` (`id_nucleo_familiar_tipo`, `nombre_tipo`) VALUES
(1, 'ENCARGADO/A'),
(2, 'ESPOSO/A'),
(3, 'HIJO/A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_auditoria`
--
ALTER TABLE `log_auditoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nucleo_familiar__asocio`
--
ALTER TABLE `nucleo_familiar__asocio`
  ADD PRIMARY KEY (`id_nucleo_familiar__asocio`),
  ADD KEY `fk_persona_encargada_idx` (`id_persona_encargada`),
  ADD KEY `fk_persona_idx` (`id_persona`);

--
-- Indexes for table `nucleo_familiar__personas`
--
ALTER TABLE `nucleo_familiar__personas`
  ADD PRIMARY KEY (`id_nucleo_familiar__persona`),
  ADD UNIQUE KEY `documento_UNIQUE` (`documento_persona`),
  ADD KEY `fk_tipo_nucleo_familar_idx` (`id_nucleo_familiar_tipo`);

--
-- Indexes for table `nucleo_familiar__tipo`
--
ALTER TABLE `nucleo_familiar__tipo`
  ADD PRIMARY KEY (`id_nucleo_familiar_tipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_auditoria`
--
ALTER TABLE `log_auditoria`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `nucleo_familiar__asocio`
--
ALTER TABLE `nucleo_familiar__asocio`
  MODIFY `id_nucleo_familiar__asocio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `nucleo_familiar__personas`
--
ALTER TABLE `nucleo_familiar__personas`
  MODIFY `id_nucleo_familiar__persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `nucleo_familiar__tipo`
--
ALTER TABLE `nucleo_familiar__tipo`
  MODIFY `id_nucleo_familiar_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nucleo_familiar__asocio`
--
ALTER TABLE `nucleo_familiar__asocio`
  ADD CONSTRAINT `fk_persona` FOREIGN KEY (`id_persona`) REFERENCES `nucleo_familiar__personas` (`id_nucleo_familiar__persona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persona_encargada` FOREIGN KEY (`id_persona_encargada`) REFERENCES `nucleo_familiar__personas` (`id_nucleo_familiar__persona`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nucleo_familiar__personas`
--
ALTER TABLE `nucleo_familiar__personas`
  ADD CONSTRAINT `fk_tipo_nucleo_familar` FOREIGN KEY (`id_nucleo_familiar_tipo`) REFERENCES `nucleo_familiar__tipo` (`id_nucleo_familiar_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
