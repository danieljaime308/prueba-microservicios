package com.example.nucleofamiliar.controllers;


import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import com.example.nucleofamiliar.entities.Persona;
import com.example.nucleofamiliar.services.AuditoriaLogService;
import com.example.nucleofamiliar.services.PersonasServices;
import com.example.nucleofamiliar.utils.FormatErrorMessage;
import com.example.nucleofamiliar.utils.GetUrl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Api
@RestController
@RequestMapping(value = "/persons")
public class PersonController {

    @Autowired
    private PersonasServices personasServices;

    @Autowired
    private AuditoriaLogService auditoriaLogService;


    /**
     * TODO REST LISTADO DE PERSONAS
     * @param tipoNucleoFamiliar NINGUNO - Muestra todas las personas habilitadas 1 - ENCARGADO/A  2 - ESPOSO/A - 2 HIJO/A
     * @return Listado de Personas
     */
    @GetMapping
    public ResponseEntity<List<Persona>> listPersonas(@RequestParam(name = "tipoNucleoFamiliar",required = false) Long tipoNucleoFamiliar){
        try{
            List<Persona> personas = new ArrayList<Persona>();
            if(tipoNucleoFamiliar == null){
                personas = personasServices.listAllPersons();
                if(personas.isEmpty()){
                    return ResponseEntity.noContent().build();
                }
            }else{
                personas = personasServices.findByPersons(NucleoFamiliarTipo.builder().id(tipoNucleoFamiliar).build());
                if(personas.isEmpty()){
                    return ResponseEntity.noContent().build();
                }
            }
            return ResponseEntity.ok(personas);
        }catch (Exception e){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * TODO REST OBTENER PERSONA
     * @param id identificador de la persona
     * @return Un Objeto de la persona
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<Persona> getPerson(@PathVariable(name = "id") long id){
        try {
            Persona p = personasServices.getPerson(id);
            if(p == null){
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(p);
        }catch (Exception e){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * TODO REST CREAR NUEVA PERSONA
     * @param persona Los datos de la Persona
     * @return Un Objeto de la Persona
     */
    @PostMapping(produces = "application/json")
    public ResponseEntity<Persona> createPerson(@Valid @RequestBody Persona persona, BindingResult result){
        try{
            if(result.hasErrors()){
                auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"Error de validacion", AuditoriaLogService.TypeLog.WARNING);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,FormatErrorMessage.formatoError(result));
            }
            persona.setDeleted(0L);
            Persona p = personasServices.createPerson(persona);
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),200,"Creacion de una persona", AuditoriaLogService.TypeLog.INFO);
            return ResponseEntity.status(HttpStatus.CREATED).body(p);
        }catch (Exception e){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * TODO REST ACTUALIZAR PERSONA
     * @param id identificador de la persona
     * @param persona Los datos de la Persona
     * @return Un Objeto de la Persona
     */
    @PutMapping(value = "{id}")
    public ResponseEntity<Persona> updatePersona(@PathVariable("id") long id, @RequestBody Persona persona){
         try{
             persona.setId(id);
             Persona p = personasServices.updatePerson(persona);
             if(p == null){
                 return ResponseEntity.notFound().build();
             }
             auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),200,"Actualizacion de una persona", AuditoriaLogService.TypeLog.INFO);
             return ResponseEntity.ok(persona);
         }catch (Exception e){
             auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
             return ResponseEntity.notFound().build();
         }
    }

    /**
     * TODO REST INHABILITAR PERSONA
     * @param id identificador de la persona
     * @return Un Objeto de la Persona
     */
    @DeleteMapping(value = "{id}")
    public ResponseEntity<Persona> deletePersona(@PathVariable("id") long id){
        try {
            Persona p = personasServices.deletePerson(id);
            if(p == null){
                return ResponseEntity.notFound().build();
            }
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),200,"Se Inhabilito de una persona", AuditoriaLogService.TypeLog.INFO);
            return ResponseEntity.ok(p);
        }catch (Exception e){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
            return ResponseEntity.notFound().build();
        }
    }


}
