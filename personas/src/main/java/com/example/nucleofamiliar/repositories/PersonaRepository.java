package com.example.nucleofamiliar.repositories;


import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import com.example.nucleofamiliar.entities.Persona;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaRepository extends CrudRepository<Persona, Long> {
    /**
     * TODO Metodo del repositorio donde consulta las personas con un tipo de nucleo familiar y que este habilitado
     * @param nucleoFamiliarTipo 1 - ENCARGADO/A  2 - ESPOSO/A - 2 HIJO/A
     * @param deleted  0 - Habilitado 1 - Inhabilitado
     * @return Lista de Personas
     */
    List<Persona> findByNucleoFamiliarTipoAndDeleted(NucleoFamiliarTipo nucleoFamiliarTipo, long deleted);

    /**
     * TODO Metodo de listado de personas habilitadas
     * @param deleted 0 - Habilitado 1 - Inhabilitado
     * @return Listado de Persona
     */
    List<Persona> findAllByDeleted(long deleted);

    /**
     * TODO Metodo de obtener la persona por documento
     * @param documento
     * @param deleted
     * @return
     */
    Persona findByDocumentoAndDeleted(String documento, long deleted);
}
