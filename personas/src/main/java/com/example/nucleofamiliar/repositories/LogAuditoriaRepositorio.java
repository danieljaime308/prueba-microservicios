package com.example.nucleofamiliar.repositories;

import com.example.nucleofamiliar.entities.LogAuditoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogAuditoriaRepositorio extends JpaRepository<LogAuditoria,Long> {
}
