package com.example.nucleofamiliar.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "nucleo_familiar__tipo")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NucleoFamiliarTipo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nucleo_familiar_tipo")
    private Long id;

    @Column(name = "nombre_tipo")
    private String nombre;
}
