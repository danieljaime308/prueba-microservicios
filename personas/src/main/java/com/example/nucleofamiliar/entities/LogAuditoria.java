package com.example.nucleofamiliar.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "log_auditoria")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LogAuditoria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String microservicio;

    @Column
    private String url;

    @Column
    private String endpoint;

    @Column(name = "http_code")
    private int httpCode;

    @Column
    private String message;

    @Column(name = "type_log")
    private String typeLog;

}
