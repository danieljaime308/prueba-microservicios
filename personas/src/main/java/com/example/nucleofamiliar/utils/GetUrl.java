package com.example.nucleofamiliar.utils;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

public class GetUrl {
    public static String getUrl(){
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        URI newUri = builder.build().toUri();
        return newUri.getHost() + ":" + newUri.getPort();
    }
    public static String getPath(){
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        builder.scheme("http");
        URI newUri = builder.build().toUri();
        return newUri.getPath();
    }
}
