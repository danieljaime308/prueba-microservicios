package com.example.nucleofamiliar.services;


import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import com.example.nucleofamiliar.entities.Persona;
import com.example.nucleofamiliar.repositories.PersonaRepository;
import com.example.nucleofamiliar.utils.GetUrl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonasServicesImpl implements PersonasServices {

    private final PersonaRepository personaRepository;

    @Autowired
    private AuditoriaLogService auditoriaLogService;

    /**
     * TODO Metodo de un servicio para obtener listado de personas habilitadas
     * @return Listado de personas
     */
    @Override
    public List<Persona> listAllPersons() {
        List<Persona> personas = personaRepository.findAllByDeleted(0);
        if (personas.isEmpty()) {
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"No se encuentra ninguna persona", AuditoriaLogService.TypeLog.WARNING);
            return null;
        }
        auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),200,"Listado de personas", AuditoriaLogService.TypeLog.INFO);
        return personas;
    }

    /**
     * TODO Metodo de un servicio para obtener una persona
     * @param id identificado de la persons
     * @return un Objeto de la persona
     */
    @Override
    public Persona getPerson(long id) {
        return personaRepository.findById(id).orElse(null);
    }

    /**
     * TODO Metodo de un servicio para crear una persona
     * @param persona los datos de la persona
     * @return un Objeto de la persona
     */
    @Override
    public Persona createPerson(Persona persona) {
        Persona p = personaRepository.findByDocumentoAndDeleted(persona.getDocumento(),0L);
        if(p != null){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"Ya existe la persona", AuditoriaLogService.TypeLog.WARNING);
            return p;
        }
        persona.setDeleted(0L);
        return personaRepository.save(persona);
    }

    /**
     * TODO Metodo de un servicio para actualizar una persona
     * @param persona los datos de la persona
     * @return un Objeto de la persona
     */
    @Override
    public Persona updatePerson(Persona persona) {
        Persona p = getPerson(persona.getId());
        if(p == null){
           return null;
        }
        p.setDocumento(persona.getDocumento());
        p.setNombre(persona.getNombre());
        p.setApellido(persona.getApellido());
        p.setCorreo(persona.getCorreo());
        p.setTelefono(persona.getTelefono());
        p.setFecha_nac(persona.getFecha_nac());
        p.setNucleoFamiliarTipo(persona.getNucleoFamiliarTipo());
        p.setDeleted(0L);
        return personaRepository.save(p);
    }

    /**
     * TODO Metodo de un servicio para inhabilitar una persona
     * @param id identificador de la persona
     * @return un Objeto de la persona
     */
    @Override
    public Persona deletePerson(long id) {
        Persona p = getPerson(id);
        if(p == null){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"No se entro el usuario", AuditoriaLogService.TypeLog.WARNING);
            return null;
        }
        p.setDeleted(1L);
        return personaRepository.save(p);
    }

    /**
     * TODO Metodo de un servicio para obtener el listado de personas por el tipo de nucleo familiar
     * @param nucleoFamiliarTipo 1 - ENCARGADO/A  2 - ESPOSO/A - 2 HIJO/A
     * @return
     */
    @Override
    public List<Persona> findByPersons(NucleoFamiliarTipo nucleoFamiliarTipo) {
        return personaRepository.findByNucleoFamiliarTipoAndDeleted(nucleoFamiliarTipo,0L);
    }

}
