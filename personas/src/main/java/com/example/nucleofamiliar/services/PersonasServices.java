package com.example.nucleofamiliar.services;



import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import com.example.nucleofamiliar.entities.Persona;

import java.util.List;

/**
 * TODO INTERFACE CON LOS METODOS DE LOS SERVICIOS
 */
public interface PersonasServices {
    List<Persona> listAllPersons();
    Persona getPerson(long id);
    Persona createPerson(Persona persona);
    Persona updatePerson(Persona persona);
    Persona deletePerson(long id);
    List<Persona> findByPersons(NucleoFamiliarTipo nucleoFamiliarTipo);
}
