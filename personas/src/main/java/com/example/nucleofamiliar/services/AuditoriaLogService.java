package com.example.nucleofamiliar.services;

import com.example.nucleofamiliar.entities.LogAuditoria;
import com.example.nucleofamiliar.repositories.LogAuditoriaRepositorio;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class AuditoriaLogService {

    public enum TypeLog {
       WARNING,
       INFO,
       ERROR,
       DEBUG
    }

    public final LogAuditoriaRepositorio logAuditoriaRepositorio;

    public void registerLog(String url , String endpoint,int httpCode,String message,TypeLog typeLog){
        LogAuditoria auditoria = LogAuditoria.builder()
                .microservicio("personas")
                .url(url)
                .endpoint(endpoint)
                .httpCode(httpCode)
                .message(message)
                .typeLog(typeLog.toString()).build();
        logAuditoriaRepositorio.save(auditoria);
    }
}
