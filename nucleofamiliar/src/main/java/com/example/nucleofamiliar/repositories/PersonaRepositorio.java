package com.example.nucleofamiliar.repositories;

import com.example.nucleofamiliar.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepositorio extends JpaRepository<Persona,Long> {
    Persona getByDocumento(String documento);
}
