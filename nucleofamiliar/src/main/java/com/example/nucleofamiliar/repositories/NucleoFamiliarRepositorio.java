package com.example.nucleofamiliar.repositories;

import com.example.nucleofamiliar.entities.NucleoFamiliarAsocio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NucleoFamiliarRepositorio extends JpaRepository<NucleoFamiliarAsocio,Long> {
     List<NucleoFamiliarAsocio> getAllByEncargado_Documento(String documento);
     NucleoFamiliarAsocio getByEncargado_DocumentoAndPersona_Documento(String docEncargado,String docAsociado);
     Long deleteByEncargado_DocumentoAndPersona_Documento(String docEncargado,String docAsociado);
}
