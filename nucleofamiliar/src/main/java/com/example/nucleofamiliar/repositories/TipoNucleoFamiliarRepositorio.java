package com.example.nucleofamiliar.repositories;

import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoNucleoFamiliarRepositorio extends JpaRepository<NucleoFamiliarTipo,Long> {
}
