package com.example.nucleofamiliar.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "nucleo_familiar__personas")
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nucleo_familiar__persona")
    private Long id;

    @NotEmpty(message = "El documento no puede estar vacio")
    @Column(name = "documento_persona")
    private String documento;

    @NotEmpty(message = "El nombre no puede estar vacio")
    @Column(name = "nombre_persona")
    private String nombre;

    @NotEmpty(message = "El apellido no puede estar vacio")
    @Column(name = "apellido_persona")
    private String apellido;

    @Column(name = "correo_persona")
    private String correo;

    @Column(name = "telefono_persona")
    private String telefono;

    @NotNull(message = "la fecha de nacimiento no puede estar vacio")
    @DateTimeFormat(pattern = "YYYY-MM-dd")
    @Column(name = "fecha_nac_persona")
    private Date fecha_nac;

    @NotNull(message = "el tipo de nucleo familiar no puede estar vacio")
    @ManyToOne
    @JoinColumn(name = "id_nucleo_familiar_tipo",referencedColumnName = "id_nucleo_familiar_tipo")
    private NucleoFamiliarTipo nucleoFamiliarTipo;

    @Column(name = "deleted")
    private Long deleted;

}
