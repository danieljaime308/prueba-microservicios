package com.example.nucleofamiliar.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


import javax.persistence.*;

@Entity
@Table(name = "nucleo_familiar__asocio")
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class NucleoFamiliarAsocio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nucleo_familiar__asocio")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_persona_encargada",referencedColumnName = "id_nucleo_familiar__persona")
    private Persona encargado;

    @ManyToOne
    @JoinColumn(name = "id_persona",referencedColumnName = "id_nucleo_familiar__persona")
    private Persona persona;

}
