package com.example.nucleofamiliar.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Getter
@Setter
@Builder
public class ResponseData<T> {
    private Date timestamp;
    private String mensaje;
    private T data;
    private int status;
}
