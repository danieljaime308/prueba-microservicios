package com.example.nucleofamiliar.utils;

import java.util.Date;

public class HttpMessage<T>{
    public ResponseData<T> getMessage(String msg,T data,int status){
        return ResponseData.<T>builder()
                .timestamp(new Date())
                .mensaje(msg)
                .data(data)
                .status(status).build();
    }
}
