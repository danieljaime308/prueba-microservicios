package com.example.nucleofamiliar.services;

import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import com.example.nucleofamiliar.entities.Persona;
import com.example.nucleofamiliar.utils.ResponseData;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface NucleoFamiliarServices {
    List<NucleoFamiliarTipo> listAllTipoNucleoFamiliar();
    List<Persona> listAllPersonPorEncargado(String documento);
    ResponseData<List<Persona>> asociarPersona(String docEncargado, String docPersona);
    ResponseData<Long> desasociarPersona(String docEncargado, String docPersona);
}
