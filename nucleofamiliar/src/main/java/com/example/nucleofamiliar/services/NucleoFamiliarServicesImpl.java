package com.example.nucleofamiliar.services;

import com.example.nucleofamiliar.entities.NucleoFamiliarAsocio;
import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import com.example.nucleofamiliar.entities.Persona;
import com.example.nucleofamiliar.repositories.NucleoFamiliarRepositorio;
import com.example.nucleofamiliar.repositories.PersonaRepositorio;
import com.example.nucleofamiliar.repositories.TipoNucleoFamiliarRepositorio;
import com.example.nucleofamiliar.utils.GetUrl;
import com.example.nucleofamiliar.utils.HttpMessage;
import com.example.nucleofamiliar.utils.ResponseData;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class NucleoFamiliarServicesImpl implements NucleoFamiliarServices {

    private final NucleoFamiliarRepositorio nucleoFamiliarRepositorio;
    private final TipoNucleoFamiliarRepositorio tipoNucleoFamiliarRepositorio;
    private final PersonaRepositorio personaRepositorio;

    @Autowired
    private AuditoriaLogService auditoriaLogService;

    @Override
    public List<NucleoFamiliarTipo> listAllTipoNucleoFamiliar() {
        List<NucleoFamiliarTipo> tipos = tipoNucleoFamiliarRepositorio.findAll();
        if(tipos.isEmpty()){
            return null;
        }
        return tipos;
    }

    /**
     * Metodo para listar las personas asociadas por encargado
     * @param documento documento del encargado
     * @return Listado de personas
     */
    @Override
    public List<Persona> listAllPersonPorEncargado(String documento) {
        List<NucleoFamiliarAsocio> list = nucleoFamiliarRepositorio.getAllByEncargado_Documento(documento);
        if(list.isEmpty()){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"No se encuentra ningun tipos de nucleo familiar", AuditoriaLogService.TypeLog.WARNING);
            return null;
        }
        List<Persona> personas = new ArrayList<Persona>();
        for (NucleoFamiliarAsocio n: list) {
            personas.add(n.getPersona());
        }
        auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),200,"Listado de tipos de nucleo familiar", AuditoriaLogService.TypeLog.INFO);
        return personas;
    }

    /**
     * Metodo para asociar una persona con el familiar
     * @param docEncargado documento del encargado
     * @param docPersona documento del familiar
     * @return respuesta HTTP
     */
    @Override
    public ResponseData<List<Persona>> asociarPersona(String docEncargado, String docPersona) {
        HttpMessage<List<Persona>> response = new HttpMessage<>();
        Persona perEncargado = this.validatePersona(docEncargado);
        Persona perAsociada = this.validatePersona(docPersona);
        List<Persona> personas = new ArrayList<>();
        if(perAsociada == null) {
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"El documento asociado no existe", AuditoriaLogService.TypeLog.WARNING);
            return response.getMessage("El documento asociado no existe", personas, HttpStatus.ACCEPTED.value());
        }
        if(perEncargado == null) {
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"El documento asociado no existe", AuditoriaLogService.TypeLog.WARNING);
            return response.getMessage("El documento del encargado no existe", personas, HttpStatus.ACCEPTED.value());
        }

        NucleoFamiliarAsocio validateAsocio = this.validateAsocio(docEncargado,docPersona);
        if(validateAsocio != null) {
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"Ya esta asociado el familiar", AuditoriaLogService.TypeLog.WARNING);
            return response.getMessage("Ya esta asociado el familiar",personas,HttpStatus.ACCEPTED.value());
        }

        NucleoFamiliarAsocio nucleo = NucleoFamiliarAsocio.builder()
                .encargado(perEncargado)
                .persona(perAsociada)
                .build();
        NucleoFamiliarAsocio newAsocio = nucleoFamiliarRepositorio.save(nucleo);
        if(newAsocio.getId() == 0) {
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,"No se pudo asociar la persona", AuditoriaLogService.TypeLog.ERROR);
            return response.getMessage("No se pudo asociar la persona",personas,HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        personas.add(perEncargado);
        personas.add(perAsociada);
        auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),200,"Se asocio correctamente", AuditoriaLogService.TypeLog.INFO);
        return response.getMessage("Se asocio correctamente",personas,HttpStatus.OK.value());
    }

    /**
     * Metodo para desasociar una persona con el familiar
     * @param docEncargado documento del encargado
     * @param docPersona documento del familiar
     * @return respuesta HTTP
     */
    @Override
    public ResponseData<Long> desasociarPersona(String docEncargado, String docPersona) {
        HttpMessage<Long> response = new HttpMessage<>();
        Persona perEncargado = this.validatePersona(docEncargado);
        Persona perAsociada = this.validatePersona(docPersona);
        if(perAsociada == null){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"El documento asociado no existe", AuditoriaLogService.TypeLog.WARNING);
            return response.getMessage("El documento asociado no existe",0L,HttpStatus.ACCEPTED.value());
        }
        if(perEncargado == null){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),202,"Ya esta asociado el familiar", AuditoriaLogService.TypeLog.WARNING);
            return response.getMessage("El documento del encargado no existe",0L,HttpStatus.ACCEPTED.value());
        }
        Long data = nucleoFamiliarRepositorio.deleteByEncargado_DocumentoAndPersona_Documento(docEncargado,docPersona);
        auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),200,"Eliminar asocio con familia", AuditoriaLogService.TypeLog.INFO);
        return response.getMessage("Se desasocio el familiar correctamente",data,HttpStatus.OK.value());
    }

    /**
     * Metodo para validar si existe la persona
     * @param documento documento de la persona
     * @return Objeto de la Persona
     */
    private Persona validatePersona(String documento){
        return personaRepositorio.getByDocumento(documento);
    }

    /**
     * Metodo para validar si existe una asociacion
     * @param docEncargado documento del encargado
     * @param docPersona documento del familiar
     * @return un objeto del asocio
     */
    private NucleoFamiliarAsocio validateAsocio(String docEncargado,String docPersona){
        return nucleoFamiliarRepositorio.getByEncargado_DocumentoAndPersona_Documento(docEncargado,docPersona);
    }


}
