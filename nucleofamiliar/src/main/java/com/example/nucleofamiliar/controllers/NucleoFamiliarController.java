package com.example.nucleofamiliar.controllers;

import com.example.nucleofamiliar.entities.NucleoFamiliarTipo;
import com.example.nucleofamiliar.entities.Persona;
import com.example.nucleofamiliar.services.AuditoriaLogService;
import com.example.nucleofamiliar.services.NucleoFamiliarServices;
import com.example.nucleofamiliar.utils.GetUrl;
import com.example.nucleofamiliar.utils.ResponseData;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@Api
@RestController
@RequestMapping(value = "/nucleofamiliar")
public class NucleoFamiliarController {

    @Autowired
    private NucleoFamiliarServices nucleoFamiliarServices;

    @Autowired
    private AuditoriaLogService auditoriaLogService;


    /**
     * TODO REST LISTADO DE TIPOS DE NUCLEO FAMILIAR
     * @return Listado de Tipos de Nucleo Familiar
     */
    @GetMapping(value = "/tipos")
    public ResponseEntity<List<NucleoFamiliarTipo>> listTipoNucleoFamiliar(){
        try{
            return ResponseEntity.ok(nucleoFamiliarServices.listAllTipoNucleoFamiliar());
        }catch (Exception e){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * TODO REST LISTA DE PERSONAS ASOCIADAS POR ENCARGADO
     * @param documento Documento del encargado
     * @return Listado de Personas
     */
    @GetMapping
    public ResponseEntity<List<Persona>> listPersonasEncargadas(@RequestParam(name = "encargado",required = false) String documento){
        try{
            List<Persona> personaList = nucleoFamiliarServices.listAllPersonPorEncargado(documento);
            if(personaList == null){
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(personaList);
        }catch (Exception e){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * TODO REST ASOCIAR PERSONA CON EL ENCARGADO DEL NUCLEO FAMILIAR
     * @param json Un Map con el documento del encargado y a la persona que se va asociar
     * @return Objeto de RespuestaData
     */
    @PostMapping
    public ResponseEntity<ResponseData<List<Persona>>> asociarPersonaNucleoFamiliar(@RequestBody Map<String, String> json){
       try{
           return ResponseEntity.ok(nucleoFamiliarServices.asociarPersona(json.get("docEncargado"),json.get("docFamiliar")));
       }catch (Exception e){
           auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
           return ResponseEntity.notFound().build();
       }
    }

    /**
     * TODO REST DESASOCIAR PERSONA CON EL ENCARGADO DEL NUCLEO FAMILIAR
     * @param json Un Map con el documento del encargado y a la persona que se va desasociar
     * @return Objeto de RespuestaData
     */
    @DeleteMapping
    public ResponseEntity<ResponseData<Long>> desasociarPersonaNucleoFamiliar(@RequestBody Map<String, String> json){
        try{
            return ResponseEntity.ok(nucleoFamiliarServices.desasociarPersona(json.get("docEncargado"),json.get("docFamiliar")));
        }catch (Exception e){
            auditoriaLogService.registerLog(GetUrl.getUrl(),GetUrl.getPath(),500,e.getMessage(), AuditoriaLogService.TypeLog.ERROR);
            return ResponseEntity.noContent().build();
        }
    }


}
