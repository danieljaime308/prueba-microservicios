package com.example.nucleofamiliar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NucleofamiliarApplication {

	public static void main(String[] args) {
		SpringApplication.run(NucleofamiliarApplication.class, args);
	}

}
